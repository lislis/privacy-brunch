(local title (love.graphics.newImage "assets/title-screen.png"))
(local prometheus (love.graphics.newImage "assets/prometheus.png"))
(local fivespices (love.graphics.newImage "assets/fivespices.png"))
(local absinth (love.graphics.newImage "assets/absinth.png"))
(local marathon (love.graphics.newImage "assets/marathon.png"))
(local fennel (love.graphics.newImage "assets/fennel.png"))

(local levels {:title {:bg title
                       :count 1
                       :coords {:x 150 :y 600 :w 150 :h 150}}
               :prometheus {:bg prometheus
                            :count 1
                            :coords {:x 20 :y 20 :w 100 :h 170 }}
               :fivespices {:bg fivespices
                            :count 1
                            :coords {:x 200 :y 200 :w 80 :h 70}}
               :absinth {:bg absinth
                         :count 1
                         :coords {:x 350 :y 450 :w 70 :h 130}}
               :marathon {:bg marathon
                          :count 9
                          :coords {:x 20 :y 600 :w 440 :h 150}}
               :end {:bg title
                     :count 1
                     :coords {:x 150 :y 600 :w 150 :h 150}}})

(var state {:level :title
            :counter 0
            :clicks []})

(fn loader []
  (love.mouse.setVisible false))

(fn draw []
  (love.graphics.setBackgroundColor 0 3 3 1)

  (let [level (. state :level)
        info (. levels level)
        bg (. info :bg)
        coords(. info :coords)]
    (love.graphics.setColor 1 1 1)
    (love.graphics.draw bg 0 0 0 1 1)
    (love.graphics.setColor 0.5 0.9 0.4)
    (love.graphics.setLineWidth 5)
    (love.graphics.rectangle :line
                             (. coords :x)
                             (. coords :y)
                             (. coords :w)
                             (. coords :h))
    (love.graphics.setColor 1 1 1)
    (if (> (# (. state :clicks)) 0)
        (each [i v (ipairs (. state :clicks))]
          (love.graphics.draw fennel (. v :x) (. v :y) 0 0.6 0.6))))

  (let [mx (love.mouse.getX)
        my (love.mouse.getY)
        offx (- mx 20)
        offy (- my 20)]
    (love.graphics.draw fennel offx offy 0 0.6 0.6)))

(fn update [dt])

(fn set-state [level]
  (tset state :level level)
  (tset state :counter 0)
  (tset state :clicks []))

;; could not get match to work??
(fn next-level [level]
  (print "level: " level)
  (if (= level :title) (set-state :prometheus))
  (if (= level :prometheus) (set-state :fivespices))
  (if (= level :fivespices) (set-state :absinth))
  (if (= level :absinth) (set-state :marathon))
  (if (= level :marathon) (set-state :end))
  level)

(fn increase-counter []
  (let [counter (. state :counter)
        level (. state :level)
        new-count (+ counter 1)
        level-count (. (. levels level) :count)]
    (print level new-count level-count "####")
    (if (= new-count level-count)
        (next-level level)
        (tset state :counter new-count))))

(fn click-in-bound [x, y]
  (let [level (. state :level)
        info (. levels level)
        lx (. (. info :coords) :x)
        ly (. (. info :coords) :y)
        lw (. (. info :coords) :w)
        lh (. (. info :coords) :h)]
    (if (and
         (and (> x lx) (< x (+ lx lw)))
         (and (> y ly) (< y (+ ly lh))))
        true
        false)))

(fn mousepressed [x y button]
  (if (= button 1)
      (when (= (click-in-bound x y) true)
        (let [clicks (. state :clicks)]
          (table.insert clicks {:x x :y y})
          (tset state :clicks clicks))
        (increase-counter))))

;; returning as lua module
{:load loader
 :draw draw
 :update update
 :mousepressed mousepressed}
